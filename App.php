<?php

namespace HotWire\Logger;

use HotWire\Framework\AbstractApp;

class App extends AbstractApp
{
    public function getName()
    {
        return 'HotWire:Logger';
    }
}
